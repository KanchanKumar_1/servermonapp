package com.server.services;

import com.google.gson.Gson;
import com.server.mon.SendMail;
import com.server.mon.ServerStats;
import com.server.mon.ServerStatsDTO;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MultivaluedMap;

/**
 * REST Web Service
 *
 * @author Kanchan
 */
@Path("/servermon")
public class ServerMonService {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ServerMonService
     */
    public ServerMonService() {
    }

    /**
     * Retrieves representation of an instance of
     * com.server.services.ServerMonService
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @Path("/stats")
    public String getServerStats() {
        try {

            ServerStats serverStats = new ServerStats();
            ServerStatsDTO serverStatsDTO = new ServerStatsDTO();

            /*Setting server stats here.*/
            serverStatsDTO.setPctCPUUtil(serverStats.getCPUUtilization());
            serverStatsDTO.setPctMemUtil(serverStats.getMemoryUtilization());
            serverStatsDTO.setTimestamp(System.currentTimeMillis());

            return new Gson().toJson(serverStatsDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @GET
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @Path("/sendMail")
    public String sendMail() {
        try {

            /*Getting parameters from request.*/
            MultivaluedMap<String, String> queryParams = context.getQueryParameters();
            String smtpHost = queryParams.getFirst("smtpHost");
            String userName = queryParams.getFirst("userName");
            String password = queryParams.getFirst("password");
            String smtpPort = queryParams.getFirst("smtpPort");
            String from = queryParams.getFirst("from");
            String to = queryParams.getFirst("to");
            String subject = queryParams.getFirst("subject");
            String bodyText = queryParams.getFirst("bodyText");

            return new SendMail().sendMail(smtpHost, userName, password, smtpPort, from, to, subject, bodyText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
