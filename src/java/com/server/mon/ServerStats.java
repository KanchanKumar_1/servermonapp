package com.server.mon;

import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;

public class ServerStats {

    	/*Initializing objects globally.*/
	HardwareAbstractionLayer hlayer = null;
	
	/*Initializing Constructor.*/
	public ServerStats() {
       hlayer = new SystemInfo().getHardware();
	}
	
	/*Getting Memory Utilization.*/
    public double getMemoryUtilization() {
         long available = hlayer.getMemory().getAvailable();
         double total = hlayer.getMemory().getTotal();
         double used = total - available;
         double usedPct = (used/total) * 100;
         return usedPct;
    }
    
    /*Getting CPU Utilization.*/
    public double getCPUUtilization() {
    	return hlayer.getProcessor().getSystemCpuLoad() * 100;
    }

	public static void main(String[] args) {
		ServerStats serverStatsInfo = new ServerStats();
		System.out.println("Memory Utilization = " + serverStatsInfo.getMemoryUtilization());
		System.out.println("CPU Utilization = " + serverStatsInfo.getCPUUtilization());


	}

}
