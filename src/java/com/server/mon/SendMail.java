package com.server.mon;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Class is responsible for sending Email
 * @author Kanchan
 */
public class SendMail {

    public String sendMail(String smtpHost, String userName, String password, String smtpPort, String from, String to, String subject, String bodyText) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.port", smtpPort);

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText("Hi,"
                    + "\n\n " + bodyText);

            Transport.send(message);

            return "Email send successfully to " + to + ".";

        } catch (Exception e) {
            return "Error while sending mail to " + to + ". Error = " + e.getMessage();
        }
        
    }

    public static void main(String[] args) {

    }
}
