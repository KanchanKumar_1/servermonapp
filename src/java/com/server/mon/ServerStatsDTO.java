
package com.server.mon;

/**
 * DTO for sending data to client GUI.
 * @author Kanchan
 */
public class ServerStatsDTO {
    
    private double pctMemUtil = 0.0;
    private double pctCPUUtil = 0.0;
    private long timestamp = 0L;

    public double getPctCPUUtil() {
        return pctCPUUtil;
    }

    public void setPctCPUUtil(double pctCPUUtil) {
        this.pctCPUUtil = pctCPUUtil;
    }

    public double getPctMemUtil() {
        return pctMemUtil;
    }

    public void setPctMemUtil(double pctMemUtil) {
        this.pctMemUtil = pctMemUtil;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ServerStatsDTO{" + "pctMemUtil=" + pctMemUtil + ", pctCPUUtil=" + pctCPUUtil + ", timestamp=" + timestamp + '}';
    }
}
