
## --- Project Information. ---
--
A Server Monitoring web application, which shows two dial chart for CPU and Memory Utilization and one line graph which show the CPU Utilization and updates on every 10 second.
It checks the Memory Utilization and send mail if used memory goes beyond 50%. However for sending mail you have to provide the necessary detail for mail configuration.
---
## Technologies Used
---
1. Java - In backend powered with Jersey REST API and oshi library.
2. Jquery, Highcharts and Javascript - In Frontend.
3. Java Mail API - For sending Mail.

## --- How to Run ----
---
Provided the war file **(Inside target folder)**, with will run by copying in any java server(like tomcat/glassfish/weblogic) webapps directory. And then accessing the app via browser as shown in screen shot. the url is as follows.
**http://localhost:8084/ServerMonApp **
---
The used jar files are:.
---
1. 14-07-2018  11:15 PM           241,622 gson-2.8.5.jar
2. 15-07-2018  02:20 AM           218,335 javax.mail-api-1.6.1.jar
3. 14-07-2018  10:00 PM         1,440,500 jna-4.5.0.jar
4. 14-07-2018  10:00 PM         2,324,986 jna-platform-4.5.0.jar
5. 14-07-2018  10:07 PM            23,645 log4j-over-slf4j-1.7.25.jar
6. 14-07-2018  09:59 PM           290,316 oshi-core-3.4.4-sources.jar
7. 14-07-2018  09:59 PM           352,477 oshi-core-3.4.4.jar
8. 14-07-2018  10:08 PM            41,203 slf4j-api-1.7.25.jar
9. 14-07-2018  10:00 PM           514,875 threetenbp-1.3.6.jar

---
The webpage contains JavaScript API which is directly imported from cdn cloud.



  